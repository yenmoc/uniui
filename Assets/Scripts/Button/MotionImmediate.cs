﻿/*******************************************************
 * Copyright (C) 2019-2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by yenmoc - phongsoyenmoc.diep@gmail.com
 
 *******************************************************/

using System;
using UnityEngine;

namespace UnityModule.UniUI
{
    [Serializable]
    public class MotionImmediate : IMotion
    {
#pragma warning disable 0649
        public Vector3 percentScaleDown = new Vector3(0.95f, 0.95f, 1f);
#pragma warning restore 0649
        public Vector3 PercentScaleDown => percentScaleDown;

        public void MotionUp(Vector3 defaultScale, RectTransform affectObject)
        {
            affectObject.localScale = defaultScale;
        }

        public void MotionDown(Vector3 defaultScale, RectTransform affectObject)
        {
            affectObject.localScale = new Vector3(defaultScale.x * PercentScaleDown.x, defaultScale.y * PercentScaleDown.y);
        }
    }
}