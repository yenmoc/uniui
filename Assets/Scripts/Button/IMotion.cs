﻿/*******************************************************
 * Copyright (C) 2019-2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by yenmoc - phongsoyenmoc.diep@gmail.com
 
 *******************************************************/

namespace UnityModule.UniUI
{
    public interface IMotion
    {
        /// <summary>
        /// percent of dedault localscale value of button when OnPointerDown
        /// </summary>
        UnityEngine.Vector3 PercentScaleDown { get; }

        /// <summary>
        /// 
        /// </summary>
        void MotionUp(UnityEngine.Vector3 defaultScale, UnityEngine.RectTransform affectObject);

        /// <summary>
        /// 
        /// </summary>
        void MotionDown(UnityEngine.Vector3 defaultScale, UnityEngine.RectTransform affectObject);
    }
}