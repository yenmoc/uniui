﻿/*******************************************************
 * Copyright (C) 2019-2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by yenmoc - phongsoyenmoc.diep@gmail.com
 
 *******************************************************/

namespace UnityModule.UniUI
{
    public interface IToggleAffect : IAffect
    {
        bool IsExpan { get; }
        UnityEngine.UI.LayoutElement LayoutElement { get; }
        UnityEngine.Vector2 ValueExpand { get; }
        UnityEngine.Vector2 ValueFlexible { get; }
        UnityEngine.Vector3 SelectedScale { get; }
        UnityEngine.Vector3 UnSelectedScale { get; }
    }
}