﻿/*******************************************************
 * Copyright (C) 2019-2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by yenmoc - phongsoyenmoc.diep@gmail.com
 
 *******************************************************/

namespace UnityModule.UniPopup
{
    public interface IUniPopupHandler
    {
        /// <summary>
        /// canvas contains popup
        /// </summary>
        UnityEngine.Canvas Canvas { get; }

        /// <summary>
        /// active popup
        /// </summary>
        void Show();

        /// <summary>
        /// deactive popup
        /// </summary>
        void Hide();

        /// <summary>
        /// update sorting order of cavas contains popup
        /// </summary>
        /// <param name="sortingOrder"></param>
        void UpdateSortingOrder(int sortingOrder);
    }
}