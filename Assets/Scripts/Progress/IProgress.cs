﻿/*******************************************************
 * Copyright (C) 2019-2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by yenmoc - phongsoyenmoc.diep@gmail.com
 
 *******************************************************/

using ExtraUniRx;
using UnityEngine;

namespace UnityModule.UniUI
{
    public interface IProgress
    {
        TenseSubject<float> Progress { get; }
        float Current { get; set; }
        float Min { get; }
        float Max { get; }
        BarDirections Direction { get; }
        RectTransform ForegroundBar { get; set; }
        RectTransform DelayedBar { get; set; }
        IMotionProgressBar MotionForegroundBar { get; }
        IMotionProgressBar MotionDelayedBar { get; }
    }
}