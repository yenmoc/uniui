﻿/*******************************************************
 * Copyright (C) 2019-2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by yenmoc - phongsoyenmoc.diep@gmail.com
 
 *******************************************************/

using System;

namespace UnityModule.UniUI
{
    public interface IProgressMode
    {
        /// <summary>
        /// return IDisposable (foregroundDid, foregroundDo, delayedDid, delayedDo)
        /// </summary>
        /// <param name="progress"></param>
        /// <returns></returns>
        (IDisposable, IDisposable, IDisposable, IDisposable) Initialized(IProgress progress);
    }
}