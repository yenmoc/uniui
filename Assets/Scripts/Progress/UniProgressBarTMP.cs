﻿/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/

using TMPro;
using UnityEngine;

// ReSharper disable ConvertToAutoProperty
// ReSharper disable InconsistentNaming
namespace UnityModule.UniUI
{
    public class UniProgressBarTMP : UniProgressBar
    {
#pragma warning disable 0649
        [SerializeField] private TextMeshProUGUI text;
#pragma warning restore 0649
        public TextMeshProUGUI Text
        {
            get => text;
            set => text = value;
        }
    }
}