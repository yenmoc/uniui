﻿/*******************************************************
 * Copyright (C) 2019-2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by yenmoc - phongsoyenmoc.diep@gmail.com
 
 *******************************************************/

namespace UnityModule.UniUI
{
    public interface IMotionProgressBar
    {
        /// <summary>
        /// delay run
        /// </summary>
        float Delay { get; }

        /// <summary>
        /// duration
        /// </summary>
        float Duration { get; }

        /// <summary>
        /// one unit value
        /// </summary>
        float OneUnitValue { get; }

        /// <summary>
        /// percent duration increase
        /// </summary>
        float PercentDurationIncrease { get; }

        /// <summary>
        /// tween update value progress
        /// </summary>
        UniTween.ITween Tween(float value);

    }
}