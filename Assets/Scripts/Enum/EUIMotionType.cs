﻿/*******************************************************
 * Copyright (C) 2019-2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by yenmoc - phongsoyenmoc.diep@gmail.com
 
 *******************************************************/

namespace UnityModule.UniUI
{
    // ReSharper disable once InconsistentNaming
    public enum EUIMotionType
    {
        Immediate = 0,
        EaseDownEaseUp = 1,
        EaseDownCurveUp = 2,
        CurveDownEaseUp = 3,
        CurveDownCurveUp = 4,
    }
}