﻿/*******************************************************
 * Copyright (C) 2019-2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by yenmoc - phongsoyenmoc.diep@gmail.com
 
 *******************************************************/

namespace UnityModule.UniUI
{
    public enum EPivot
    {
        UpperLeft = 1,
        UpperCenter = 2,
        UpperRight = 3,
        MiddleLeft = 4,
        MiddleCenter = 5,
        MiddleRight = 6,
        LowerLeft = 7,
        LowerCenter = 8,
        LowerRight = 9,
    }
}