﻿/*******************************************************
 * Copyright (C) 2019-2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by yenmoc - phongsoyenmoc.diep@gmail.com
 
 *******************************************************/

namespace UnityModule.UniUI
{
    public interface IAffect
    {
        /// <summary>
        /// default scale
        /// </summary>
        UnityEngine.Vector3 DefaultScale { get; set; }

        /// <summary>
        /// is affect to self
        /// </summary>
        bool IsAffectToSelf { get; }

        /// <summary>
        /// object affect if IsAffectToSelf equal false.
        /// </summary>
        UnityEngine.RectTransform AffectObject { get; }
    }
}