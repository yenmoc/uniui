﻿/*******************************************************
 * Copyright (C) 2019-2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by yenmoc - phongsoyenmoc.diep@gmail.com
 
 *******************************************************/

namespace UnityModule.UniUI
{
    public interface IUniDynamic
    {
        /// <summary>
        /// pivot of RectTransform
        /// </summary>
        EPivot Pivot { get; }

        /// <summary>
        /// is using motion when click button
        /// </summary>
        bool IsMotion { get; }

        /// <summary>
        /// is release only set true when OnPointerUp called
        /// </summary>
        bool IsRelease { get; }

        /// <summary>
        /// make sure OnPointerClick is called on the condition of IsRelease, only set tru when OnPointerExit called
        /// </summary>
        bool IsPrevent { get; }

        /// <summary>
        /// motion type
        /// </summary>
        EUIMotionType MotionType { get; }
    }
}