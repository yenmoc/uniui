﻿/*******************************************************
 * Copyright (C) 2019-2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by yenmoc - phongsoyenmoc.diep@gmail.com
 
 *******************************************************/

using UnityModule.EditorUtility;

// ReSharper disable InconsistentNaming
namespace UnityModule.UniUI.Editor
{
    [UnityEditor.CustomEditor(typeof(UniButtonTMP), true)]
    [UnityEditor.CanEditMultipleObjects]
    public class UniButtonTMPEditor : UniButtonEditor
    {
        protected override void DrawInspector()
        {
            EditorUtil.DrawSeparator();
            EditorUtil.SerializeField(serializedObject, "pivot");
            EditorUtil.SerializeField(serializedObject, "text");
            Draw();
        }
    }
}