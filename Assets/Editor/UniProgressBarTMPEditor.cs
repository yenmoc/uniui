﻿/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/

using UnityEditor;
using UnityEngine;
using UnityModule.EditorUtility;

namespace UnityModule.UniUI.Editor
{
    [CustomEditor(typeof(UniProgressBarTMP), true)]
    [CanEditMultipleObjects]
    // ReSharper disable once InconsistentNaming
    public class UniProgressBarTMPEditor : UniProgressBarEditor
    {
        protected override void Draw()
        {
            EditorUtil.DrawSeparator(1.5f);
            EditorUtil.SerializeField(serializedObject, "direction");
            EditorUtil.SerializeField(serializedObject, "mode");
            hasDelayBar = EditorUtil.SerializeField(serializedObject, "hasDelayBar");
            EditorUtil.SerializeField(serializedObject, "text");
            GUILayout.Space(2);
            EditorUtil.SerializeField(serializedObject, "foregroundBar");
            if (hasDelayBar.boolValue)
            {
                EditorUtil.SerializeField(serializedObject, "delayedBar");
            }

            GUILayout.Space(8);
            EditorUtil.SerializeField(serializedObject, "curveForegroundBar", "ForegroundBar Use Curve");

            if (hasDelayBar.boolValue)
            {
                EditorUtil.SerializeField(serializedObject, "curveDelayBar", "DelayBar Use Curve");
            }

            EditorUtil.SerializeField(serializedObject, "_foregroundMotion");
            if (hasDelayBar.boolValue)
            {
                EditorUtil.SerializeField(serializedObject, "_delaybarMotion");
            }
        }
    }
}