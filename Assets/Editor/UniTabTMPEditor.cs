﻿/*******************************************************
 * Copyright (C) 2019-2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by yenmoc - phongsoyenmoc.diep@gmail.com
 
 *******************************************************/

using UnityModule.EditorUtility;

namespace UnityModule.UniUI.Editor
{
    [UnityEditor.CustomEditor(typeof(UniTapTMP), true)]
    // ReSharper disable once InconsistentNaming
    public class UniTabTMPEditor : UniTabEditor
    {
        protected override void DrawInspector()
        {
            EditorUtil.DrawSeparator();
            EditorUtil.SerializeField(serializedObject, "pivot");
            EditorUtil.SerializeField(serializedObject, "text");
            EditorUtil.SerializeField(serializedObject, "activeObjects");
            EditorUtil.SerializeField(serializedObject, "deactiveObjects");
            Draw();
        }
    }
}