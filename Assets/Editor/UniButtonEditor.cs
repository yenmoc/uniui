﻿/*******************************************************
 * Copyright (C) 2019-2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by yenmoc - phongsoyenmoc.diep@gmail.com
 
 *******************************************************/

using UnityEditor;
using UnityEngine;
using UnityModule.EditorUtility;

namespace UnityModule.UniUI.Editor
{
    [CustomEditor(typeof(UniButton), true)]
    [CanEditMultipleObjects]
    public class UniButtonEditor : UnityEditor.UI.ButtonEditor
    {
        private SerializedProperty _isMotion;
        private SerializedProperty _isAffectToSelf;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            DrawInspector();
        }

        protected virtual void DrawInspector()
        {
            EditorUtil.DrawSeparator();
            EditorUtil.SerializeField(serializedObject, "pivot");
            Draw();
        }

        protected void Draw()
        {
            _isMotion = EditorUtil.SerializeField(serializedObject, "isMotion");
            if (_isMotion.boolValue)
            {
                _isAffectToSelf = EditorUtil.SerializeField(serializedObject, "isAffectToSelf", "Affect To Self");
                if (!_isAffectToSelf.boolValue)
                {
                    EditorUtil.SerializeField(serializedObject, "affectObject", "Object Affect Transform");
                    EditorGUILayout.Space();
                }

                EditorUtil.SerializeField(serializedObject, "motionType");
                EditorUtil.SerializeField(serializedObject, "_motion");
            }
            
            Repaint();
            EditorGUILayout.Space();
            serializedObject.ApplyModifiedProperties();
        }
    }
}