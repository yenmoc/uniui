# UniUi

## Requirement
	-[Verdaccio] for local

## Dependency
	-"com.yenmoc.unitween"
    -"com.yenmoc.utility"

## Installation

```bash
"com.yenmoc.uniui":"https://gitlab.com/yenmoc/uniui"
or
npm publish --registry http://localhost:4873
```

## Content
	-Button - `UniButton` - `UniButtonTMP`
	-Propup - `UniPopup`
	-Progress - `UniProgress` - `UniProgressTMP`
	-Scroll - `FancyScroll`
	-Toggle - `UniTab` - `UniTabTMP`
