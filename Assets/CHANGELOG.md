# Changelog
All notable changes to this project will be documented in this file.

## [0.1.8] - 21-01-2020
### Changed
	-update dependency utility to v0.1.14
	-update fancyscrollview to v1.8.5
